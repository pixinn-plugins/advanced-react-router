import React from 'react';
import { RouteProps } from 'react-router-dom';
declare type AdjustedAuthLevels = AuthLevels | 'public';
interface RouterContext {
    /** Current user authorization level */
    currentLevel: AdjustedAuthLevels;
    /** Hierarchy of authorization levels */
    hierarchy?: AdjustedAuthLevels[];
    /**
     * Is user logged in
     * Default: false
     */
    loggedIn?: boolean;
    /** Route to redirect when user is not logged and tries to access not public route */
    loginRoute: string;
    /** Route to redirect when user auth level is invalid */
    unauthorizedRoute?: string;
}
interface IProps extends RouteProps {
    /**
     * Route authorization level.
     * Default: public
     */
    level?: AdjustedAuthLevels;
    /**
     * Defines if user auth level must be exact as given.
     * Default: false
     */
    exactLevel?: boolean;
}
/**
 * React Router with applied authorization properties.
 */
export declare const Route: React.FC<IProps>;
/**
 * Auth router context provider
 */
export declare const AuthRouterProvider: React.FC<RouterContext>;
/**
 * Auth router context HOC
 */
export declare const withAuthRouter: (options: RouterContext) => <T extends object>(Component: React.ComponentType<T>) => (props: T) => JSX.Element;
/**
 * Returns current user auth level
 */
export declare function useCurrentLevel(): AdjustedAuthLevels;
/**
 * Checks if user has proper authorization level
 * @param level Desired authorization level
 * @param exact Check for exact authorization level match
 */
export declare function useIsAuthorizedFor(level: AdjustedAuthLevels, exact?: boolean): boolean;
/**
 * Conditional container for showing or hiding elements based on auth level.
 */
export declare const AuthContainer: React.FC<AuthContainer>;
interface AuthContainer {
    level: AdjustedAuthLevels;
    exact?: boolean;
}
export {};
