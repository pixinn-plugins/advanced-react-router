"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthContainer = exports.useIsAuthorizedFor = exports.useCurrentLevel = exports.withAuthRouter = exports.AuthRouterProvider = exports.Route = void 0;
const react_1 = __importDefault(require("react"));
const react_router_dom_1 = require("react-router-dom");
const routerContext = react_1.default.createContext(undefined);
/**
 * React Router with applied authorization properties.
 */
exports.Route = (_a) => {
    var { level = 'public' } = _a, props = __rest(_a, ["level"]);
    return level === 'public' && !props.exactLevel ? (react_1.default.createElement(react_router_dom_1.Route, Object.assign({}, props))) : (react_1.default.createElement(_AuthRoute, Object.assign({ level: level }, props)));
};
const _AuthRoute = (_a) => {
    var { level = 'public' } = _a, props = __rest(_a, ["level"]);
    const ctx = react_1.default.useContext(routerContext);
    if (props.exactLevel && level === ctx.currentLevel) {
        return react_1.default.createElement(react_router_dom_1.Route, Object.assign({}, props));
    }
    if (!ctx.loggedIn) {
        return (react_1.default.createElement(react_router_dom_1.Redirect, { to: {
                pathname: ctx.loginRoute,
                state: {
                    redirectedFrom: location.pathname
                }
            }, push: false }));
    }
    if ((props.exactLevel || !ctx.hierarchy) && ctx.currentLevel !== level ||
        !props.exactLevel && ctx.hierarchy && ctx.hierarchy.indexOf(ctx.currentLevel) < ctx.hierarchy.indexOf(level)) {
        return react_1.default.createElement(react_router_dom_1.Redirect, { to: ctx.unauthorizedRoute || ctx.loginRoute, push: false });
    }
    return react_1.default.createElement(react_router_dom_1.Route, Object.assign({}, props));
};
/**
 * Auth router context provider
 */
exports.AuthRouterProvider = (_a) => {
    var { children } = _a, props = __rest(_a, ["children"]);
    return react_1.default.createElement(routerContext.Provider, { value: props }, children);
};
/**
 * Auth router context HOC
 */
exports.withAuthRouter = (options) => (Component) => (props) => react_1.default.createElement(routerContext.Provider, { value: options },
    react_1.default.createElement(Component, Object.assign({}, props)));
/**
 * Returns current user auth level
 */
function useCurrentLevel() {
    return react_1.default.useContext(routerContext).currentLevel;
}
exports.useCurrentLevel = useCurrentLevel;
/**
 * Checks if user has proper authorization level
 * @param level Desired authorization level
 * @param exact Check for exact authorization level match
 */
function useIsAuthorizedFor(level, exact) {
    var _a;
    const ctx = react_1.default.useContext(routerContext);
    return exact ? ctx.currentLevel !== level : ((_a = ctx.hierarchy) === null || _a === void 0 ? void 0 : _a.length) ? ctx.hierarchy.indexOf(ctx.currentLevel) < ctx.hierarchy.indexOf(level) : ctx.currentLevel !== level;
}
exports.useIsAuthorizedFor = useIsAuthorizedFor;
/**
 * Conditional container for showing or hiding elements based on auth level.
 */
exports.AuthContainer = props => 
// tslint:disable-next-line:react-hooks-nesting
useIsAuthorizedFor(props.level, props.exact) ? props.children : null;
